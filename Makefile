CC = icpc
CFLAGS = 
COPTFLAGS = -O3 -g -openmp
LDFLAGS =

sudoku-omp: sudoku.o
	$(CC) $(COPTFLAGS) -o $@ $^

%.o: %.cc
	$(CC) $(CFLAGS) $(COPTFLAGS) -o $@ -c $<

clean:
	rm -f core *.o *~

# eof
